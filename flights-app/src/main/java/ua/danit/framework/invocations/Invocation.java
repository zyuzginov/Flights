package ua.danit.framework.invocations;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import javax.ws.rs.QueryParam;

public class Invocation {
  private Class<?> clazz;
  private Method method;
  private String url;

  private Annotation httpMethod;
  private List<QueryParam> queryParamList;

  /**
   * This constructor creates an object with parameters Path.
   */

  public Invocation(Class<?> clazz, Method method, String url) {
    this.method = method;
    this.clazz = clazz;
    this.url = url;
  }

  /**
   * This constructor creates an object with parameters Get or Post.
   */

  public Invocation(Class<?> clazz, Method method, String url, Annotation httpMethod) {
    this.url = url;
    this.clazz = clazz;
    this.method = method;
    this.httpMethod = httpMethod;
  }


  /**
   * This constructor creates an object with parameters QueryParam.
   */

  public Invocation(Class<?> clazz, Method method, String url, Annotation httpMethod,
                    List<QueryParam> queryParamList) {
    this.clazz = clazz;
    this.method = method;
    this.url = url;
    this.httpMethod = httpMethod;

    this.queryParamList = queryParamList;

  }

  /**
   * This method return a list contains a QueryParam.
   */

  public List<QueryParam> getQueryParamList() {
    return queryParamList;
  }


  public Annotation getHttpMethod() {
    return httpMethod;
  }

  public String getUrl() {
    return url;
  }


  /**
   * This method a calls invoke without parameters.
   */

  public String invoke() {
    try {
      return (String) method.invoke(clazz.newInstance(), null);
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    } catch (InvocationTargetException e) {
      e.printStackTrace();
    } catch (InstantiationException e) {
      e.printStackTrace();
    }

    return "Error 404";
  }

  /**
   * This method a calls invoke with parameters.
   */

  public String invoke(String str) {
    try {
      return (String) method.invoke(clazz.newInstance(), str);
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    } catch (InvocationTargetException e) {
      e.printStackTrace();
    } catch (InstantiationException e) {
      e.printStackTrace();
    }

    return "Error 404";
  }
}
